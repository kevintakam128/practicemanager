from flask import request, jsonify, Blueprint
from flask import current_app as app
import requests

from patient.domain.patient import Patient
from flask import Flask, g, request
from flask_oidc import OpenIDConnect
import json
from jose import jwt
patient_bp = Blueprint('patient', __name__)




oidc = OpenIDConnect()


def has_doctor_role(decoded_token):
    # Vérifie si le rôle 'docteur' est présent dans les rôles du token

    return 'doctor' in decoded_token.get('realm_access', {}).get('roles', [])

@patient_bp.route('/patients', methods=['POST'])

@oidc.accept_token() 


def create_patient():   
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        created_patient = patient_service.create_patient(Patient(**patient_data))

        return jsonify(created_patient.to_dict()), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400


@patient_bp.route('/patients', methods=['GET'])
@oidc.accept_token()
def get_all_patients():
    try:
        patient_service = app.config['patient_service']
        all_patients = patient_service.get_all_patients()

        # Convertir la liste des patients en une liste de dictionnaires
        patients_data = [patient.to_dict() for patient in all_patients]

        return jsonify(patients_data), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
    
    
@patient_bp.route('/patients/<int:patient_id>', methods=['GET'])
@oidc.accept_token()
def get_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        one_patient = patient_service.get_patient(patient_id)

        return jsonify(one_patient.to_dict()), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500
    
@patient_bp.route('/patients/<int:patient_id>', methods=['PUT'])
@oidc.accept_token()
def update_patient(patient_id):
    try:
        patient_data = request.get_json()
        patient_service = app.config['patient_service']
        updated_patient = patient_service.update_patient(patient_id, Patient(**patient_data))

        return jsonify(updated_patient), 201
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@patient_bp.route('/patients/<int:patient_id>', methods=['DELETE'])
@oidc.accept_token()
def delete_patient(patient_id):
    try:
        patient_service = app.config['patient_service']
        deleted_patient = patient_service.delete_patient(patient_id)

        return jsonify(deleted_patient), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 500