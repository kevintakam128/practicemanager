from datetime import datetime
from patient.infrasctructure.patient_entity import PatientEntity
from patient.domain.patient import Patient


class PatientService:
    def __init__(self, patient_repository):
        self.patient_repository = patient_repository

    def create_patient(self, patient: Patient) -> PatientEntity:
        required_fields = ['first_name', 'last_name', 'date_of_birth', 'social_security_number']

        if any(not getattr(patient, field, None) for field in required_fields):
            # logging.info(f'Patient data is missing required fields: {required_fields}')
            raise ValueError('Patient first name is required')

        if patient.date_of_birth != datetime.strptime(patient.date_of_birth, '%Y-%m-%d') \
                .strftime('%Y-%m-%d'):
            raise ValueError('Patient date of birth is invalid')

        created_patient = self.patient_repository.add_patient(patient)
        return created_patient

    
    
    def get_patient(self, patient_id):
        return self.patient_repository.get_patient(patient_id)
    
    def get_all_patients(self):
        return self.patient_repository.get_all_patients()
    
    def delete_patient(self, patient_id):
        existing_patient = self.patient_repository.get_patient(patient_id)
        if not existing_patient:
            raise ValueError(f'Patient with ID {patient_id} not found')

        self.patient_repository.delete_patient(patient_id)
        return f'Patient with ID {patient_id} deleted successfully'
    
    def update_patient(self, patient_id, patient_data):
        existing_patient = self.patient_repository.get_patient(patient_id)
        if not existing_patient:
            raise ValueError(f'Patient with ID {patient_id} not found')

        # Effectuez les mêmes vérifications que pour la création pour garantir la validité des données.
        required_fields = ['first_name', 'last_name', 'date_of_birth', 'social_security_number']
        if any(not getattr(patient_data, field, None) for field in required_fields):
            raise ValueError('Patient first name is required')
        if getattr(patient_data,'date_of_birth',None) != datetime.strptime(getattr(patient_data,'date_of_birth',None), '%Y-%m-%d') \
                .strftime('%Y-%m-%d'):
            raise ValueError('Patient date of birth is invalid')

        # Mettez à jour les données du patient existant avec les nouvelles données.
        updated_patient_data = {
            'first_name': getattr(existing_patient, 'first_name', None),
            'last_name': getattr(existing_patient, 'last_name', None),
            'date_of_birth': getattr(existing_patient, 'date_of_birth', None),
            'social_security_number': getattr(existing_patient, 'social_security_numbe', None)
        }

        updated_patient = self.patient_repository.update_patient(patient_id, updated_patient_data)
        return updated_patient