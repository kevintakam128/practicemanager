from flask import Flask, Blueprint, jsonify
from flask import Flask, g, request
from flask_oidc import OpenIDConnect
from jose import jwt
import requests


from patient.application.patient_ressource import patient_bp
from patient.application.patient_service import PatientService
from patient.infrasctructure.patient_repository import PatientRepository
import json


def create_app():
    app = Flask(__name__)

    app.config.update({
        'SECRET_KEY': 'SomethingNotEntirelySecret',
        'TESTING': True,
        'DEBUG': True,
        'OIDC_CLIENT_SECRETS': 'client_secrets.json',
        'OIDC_ID_TOKEN_COOKIE_SECURE': False,
        'OIDC_USER_INFO_ENABLED': True,
        'OIDC_OPENID_REALM': 'practiceManager',
        'OIDC_SCOPES': ['openid', 'email', 'profile'],
        'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post'
    })

    

    
    oidc = OpenIDConnect(app)
    
    
    @app.route('/')
    def hello_world():
        if oidc.user_loggedin:
            return ('Hello, %s, <a href="/private">See private</a> '
                    '<a href="/logout">Log out</a>') % \
                oidc.user_getfield('preferred_username')
        else:
            return 'Welcome anonymous, <a href="/private">Log in</a>'
    def get_public_key():
    # You should replace 'YOUR_IDP_PUBLIC_KEY_URL' with the actual URL where the public key can be retrieved
        public_key_url = 'J_qEP0McBZ8K6xlFxM6acCctLUe7wWhTTc8Ohf51j7Y'
        full_url = 'http://' + public_key_url

        #jwks = requests.get(full_url,verify=False).json()
        try:
            response = requests.get(full_url, verify=False)
            response.raise_for_status()  # Raise an HTTPError for bad responses

            jwks = response.json()
            public_key = jwks['keys'][0]['your_key_field']  # Replace with the actual key field

            return public_key

        except requests.exceptions.RequestException as e:
            print(f"Error in the request: {e}")

        except requests.exceptions.HTTPError as e:
            print(f"HTTP error: {e}")
            print(f"Response content: {response.content}")

        except requests.exceptions.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
            print(f"Response content: {response.content}")

        except KeyError as e:
            print(f"Key error: {e}")
            print(f"JWKS response: {jwks}")

        return None
    @app.route('/private')
    @oidc.require_login
    def hello_me():
        public=get_public_key()
        """Example for protected endpoint that extracts private information from the OpenID Connect id_token.
            Uses the accompanied access_token to access a backend service.
        """
          # Retrieve the public key for decoding the JWT
        info = oidc.user_getinfo(['preferred_username', 'email', 'sub'])

        username = info.get('preferred_username')
        email = info.get('email')
        user_id = info.get('sub')

        if user_id:
            try:
                access_token = oidc.get_access_token()
                print('access_token=<%s>' % access_token)
                headers = {'Authorization': 'Bearer %s' % (access_token)}
                
                #greeting = requests.get('http://localhost:8080/', headers=headers).text
                decoded_token = jwt.decode(access_token,public,algorithms=['RS256'], options={'verify_signature': False})
                print(decoded_token)
                realm_access_roles = decoded_token.get('realm_access', {}).get('roles', [])
                resource_access_roles = decoded_token.get('resource_access', {}).get('your-client-id', {}).get('roles', [])
                user_roles = realm_access_roles + resource_access_roles

        # Check if the user has the role 'doctor'
                if 'doctor' in user_roles:
                 return jsonify({'hello': 'Welcome Doctor %s' % username})
                else:
                  return jsonify({'error': 'Access forbidden. Not a doctor.'}), 403
              
            except:
                print("Could not access greeting-service")
                greeting = "Hello %s" % username
                            # Utilisation des informations du token

              
            return ("""%s your email is %s and your user_id is %s!
                   <ul>
                     <li><a href="/">Home</a></li>
                     <li><a href="//localhost:8080/realms/package_manager/account?referrer=flask-app&referrer_uri=http://localhost:5000/private&">Account</a></li>
                    </ul>""" %
                (greeting,email, user_id))
        
   

    @app.route('/hello', methods=['GET'])
    @oidc.accept_token()
    
    def hello_api():
        
        """OAuth 2.0 protected API endpoint accessible via AccessToken"""
        token = request.headers.get('Authorization').split(' ')[1]
        claim = jwt.get_unverified_claims(token)
        preferred_username = claim.get('preferred_username')

        return json.dumps({'hello': 'Welcome %s' % preferred_username})

    @app.route('/logout')
    def logout():
        """Performs local logout by removing the session cookie."""

        oidc.logout()
        return 'Hi, you have been logged out! <a href="/">Return</a>'

    # Configuration settings, routes, middleware, etc. can be added here
    patient_repository = PatientRepository()
    patient_service = PatientService(patient_repository)
    app.config['patient_service'] = patient_service

    app.register_blueprint(patient_bp)

    return app


if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)