import unittest
import unittest.mock as mock



from patient.application.patient_service import PatientService


class TestPatientService(unittest.TestCase):
    def setUp(self):
        self.mock_repository = mock.Mock()
        self.patient_service = PatientService(self.mock_repository)

    def test_create_patient(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }
    
        self.mock_repository.add_patient.return_value = {'id': 1, **patient_data}

        new_patient = self.patient_service.create_patient(patient_data)

        self.assertIsNotNone(new_patient)
        self.assertEqual(new_patient['id'], 1)
        self.assertEqual(new_patient['first_name'], 'John')
        self.assertEqual(new_patient['last_name'], 'Doe')
        self.assertEqual(new_patient['date_of_birth'], '1980-01-01')
        self.assertEqual(new_patient['social_security_number'], '123456789012345')

        self.mock_repository.add_patient.assert_called_once_with(patient_data)

    def create_patient_with_missing_value_raises_value_error(self, missing_field):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        del patient_data[missing_field]

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

    def test_create_patient_with_missing_first_name(self):
        self.create_patient_with_missing_value_raises_value_error('first_name')

    def test_create_patient_with_missing_last_name(self):
        self.create_patient_with_missing_value_raises_value_error('last_name')

    def test_create_patient_with_missing_date_of_birth(self):
        self.create_patient_with_missing_value_raises_value_error('date_of_birth')

    def test_create_patient_with_missing_social_security_number(self):
        self.create_patient_with_missing_value_raises_value_error('social_security_number')

    def test_create_patient_with_invalid_birth_date(self):
        patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-13-01',
            'social_security_number': '123456789012345'
        }

        with self.assertRaises(ValueError):
            self.patient_service.create_patient(patient_data)

        self.mock_repository.add_patient.assert_not_called()


'''
def test_get_all_patients(self):
        # Données simulées pour la liste de patients
        patients_data = [
            {'id': 1, 'first_name': 'Alice', 'last_name': 'Smith', 'date_of_birth': '1975-03-15', 'social_security_number': '987654321012345'},
            {'id': 2, 'first_name': 'Bob', 'last_name': 'Johnson', 'date_of_birth': '1982-05-20', 'social_security_number': '876543210123456'},
            # Ajoutez d'autres patients au besoin
        ]

        # Configurer le comportement simulé du repository lors de la récupération de la liste de patients
        self.mock_repository.get_all_patients.return_value = patients_data

        # Appeler la méthode get_all_patients du service
        all_patients = self.patient_service.get_all_patients()

        # Assurer que la liste de patients retournée correspond aux données simulées
        self.assertEqual(all_patients, patients_data)

        # Assurer que la méthode get_all_patients du repository a été appelée
        self.mock_repository.get_all_patients.assert_called_once()

def test_search_patient(self):
        # Données simulées pour la recherche d'un patient
        search_query = 'Alice'
        matching_patient_data = {'id': 1, 'first_name': 'Alice', 'last_name': 'Smith', 'date_of_birth': '1975-03-15', 'social_security_number': '987654321012345'}

        # Configurer le comportement simulé du repository lors de la recherche d'un patient
        self.mock_repository.search_patient.return_value = matching_patient_data

        # Appeler la méthode search_patient du service
        found_patient = self.patient_service.search_patient(search_query)

        # Assurer que le patient trouvé correspond aux données simulées
        self.assertEqual(found_patient, matching_patient_data)

        # Assurer que la méthode search_patient du repository a été appelée avec la requête correcte
        self.mock_repository.search_patient.assert_called_once_with(search_query)

def test_update_patient(self):
        # Données d'un patient existant
        existing_patient_data = {
            'id': 1,
            'first_name': 'Alice',
            'last_name': 'Smith',
            'date_of_birth': '1975-03-15',
            'social_security_number': '987654321012345'
        }

        # Nouvelles données pour la mise à jour
        updated_patient_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'date_of_birth': '1980-01-01',
            'social_security_number': '123456789012345'
        }

        # Configurer le comportement simulé du repository lors de la récupération du patient existant
        self.mock_repository.get_patient.return_value = {**existing_patient_data}

        # Configurer le comportement simulé du repository lors de la mise à jour du patient
        self.mock_repository.update_patient.return_value = {'id': 1, **updated_patient_data}

        # Appeler la méthode update_patient avec l'ID du patient et les nouvelles données
        updated_patient = self.patient_service.update_patient(existing_patient_data['id'], updated_patient_data)

        # Assurer que le patient mis à jour est correct
        self.assertIsNotNone(updated_patient)
        self.assertEqual(updated_patient['id'], 1)
        self.assertEqual(updated_patient['first_name'], 'John')
        self.assertEqual(updated_patient['last_name'], 'Doe')
        self.assertEqual(updated_patient['date_of_birth'], '1980-01-01')
        self.assertEqual(updated_patient['social_security_number'], '123456789012345')

        # Assurer que la méthode get_patient du repository a été appelée avec l'ID correct
        self.mock_repository.get_patient.assert_called_once_with(existing_patient_data['id'])

        # Assurer que la méthode update_patient du repository a été appelée avec les bonnes données
        self.mock_repository.update_patient.assert_called_once_with(existing_patient_data['id'], updated_patient_data)
'''
if __name__ == '__main__':
    unittest.main()